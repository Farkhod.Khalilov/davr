package com.example.davr.security;

import com.example.davr.dto.CustomUserDetails;
import com.example.davr.entity.Users;
import com.example.davr.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService  implements UserDetailsService {

    private final UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users= usersRepository.findByUsernameAndDeletedFalse(username).orElseThrow(()->
                new UsernameNotFoundException(username));
        return CustomUserDetails.of(users);




    }
}
