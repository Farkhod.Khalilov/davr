package com.example.davr.service;

import com.example.davr.dto.ChatDto;
import com.example.davr.dto.ChatFormDto;
import com.example.davr.dto.CustomUserDetails;
import com.example.davr.entity.Chat;
import com.example.davr.entity.Users;
import com.example.davr.repository.ChatRepository;
import com.example.davr.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor

public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    private final UsersRepository usersRepository;

    @Override
    public ChatDto getChat(ChatFormDto dto) {
//        Chat chat = new Chat();
//        Optional<Users> users = usersRepository.findByUsernameAndDeletedFalse(SecurityContextHolder.getContext()
//                .getAuthentication().getName());
//        return null;

        Users receiverUser = usersRepository.findById(dto.getReceiverId()).orElseThrow(() -> new RuntimeException("User not found!"));
        Users senderUser = usersRepository.findById(dto.getSenderId()).orElseThrow(() -> new RuntimeException("sender not found!"));
//        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication();

        Optional<Chat> chatOptional = chatRepository.findChat(senderUser, receiverUser);
        return ChatDto.toDto(chatOptional.orElseGet(() -> chatRepository.save(new Chat("Private Chat", Set.of(senderUser, receiverUser)))));
    }


    @Override
    public ChatDto update(ChatDto dto, Long id) {
        return null;
        // TODO: 12/05/22 yozish kerak logic qilib

    }

    @Override
    public ChatDto getOne(Long id) {
        return null;
    }

    @Override
    public List<ChatDto> findAll() {
        return null;
    }

    @Override
    public void delete(Long id) {

    }


}
