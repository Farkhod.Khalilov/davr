package com.example.davr.service;

import com.example.davr.dto.UsersDto;
import com.example.davr.dto.UsersForm;

import java.util.List;

public interface UsersService {

    UsersDto add(UsersForm form);

    UsersDto update(UsersForm form,Long id);

    UsersDto changePassword(Long id, UsersForm dto);

    UsersDto getOne(Long id);

    List<UsersDto> findAll();

    void delete(Long id);


}
