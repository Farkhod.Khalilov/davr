package com.example.davr.service;

import com.example.davr.dto.ChatDto;
import com.example.davr.dto.ChatFormDto;

import java.util.List;

public interface ChatService {

    ChatDto getChat(ChatFormDto dto);

    ChatDto update(ChatDto dto,Long id);

    ChatDto getOne(Long id);

    List<ChatDto>findAll();

    void delete(Long id);




}
