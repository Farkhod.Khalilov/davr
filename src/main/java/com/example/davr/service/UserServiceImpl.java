package com.example.davr.service;

import com.example.davr.dto.UsersDto;
import com.example.davr.dto.UsersForm;
import com.example.davr.entity.Users;
import com.example.davr.enums.UserRole;
import com.example.davr.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService {

    private final UsersRepository repository;
    private final BCryptPasswordEncoder encoder;


    @Override
    public UsersDto add(UsersForm form) {
//        Role role = roleRepository.findByName(com.example.davr.enums.UserRole.MODERATOR.name());//TODO findByName ichida String name qabul qiladi biza enumda MODERATORni chaqirib olinib ishlatildi
        if (repository.existsByUsername(form.getUsername())) throw new RuntimeException("Username already used!");
        Users user = UsersForm.toEntity(form);
        user.setPassword(encoder.encode(form.getPassword()));
        user.setRole(UserRole.MODERATOR);
        return UsersDto.toDto(repository.save(user));
    }

    @Override
    public UsersDto update(UsersForm form, Long id) {
        Users users = get(id);
        users.setPassword(encoder.encode(form.getPassword()));//TODO encoderga set qildik
        users.setUsername(form.getUsername());
        return UsersDto.toDto(repository.save(users));
    }

    @Override
    public UsersDto changePassword(Long id, UsersForm dto) {
        Users users = get(id);
        users.setPassword(encoder.encode(dto.getPassword()));
        return UsersDto.toDto(repository.save(users));

    }

    @Override
    public UsersDto getOne(Long id) {
        return UsersDto.toDto(get(id));
    }

    @Override
    public List<UsersDto> findAll() {
        return repository.findAll()
                .stream()
                .map(UsersDto::toDto)
                .collect(Collectors
                        .toList());

    }

    @Override
    public void delete(Long id) {
        Users users = get(id);
        users.setDeleted(true);
        repository.save(users);

    }

    private Users get(Long id) {
        Optional<Users> optionalUsers = repository.findById(id);
        if (optionalUsers.isPresent()) {
            return optionalUsers.get();
        } else {
            throw new RuntimeException("User not found" + id);
        }
    }
}