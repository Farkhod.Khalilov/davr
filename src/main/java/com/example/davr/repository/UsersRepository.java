package com.example.davr.repository;

import com.example.davr.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UsersRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByUsernameAndDeletedFalse(String username);

    Boolean existsByUsername(String username);


}
