package com.example.davr.repository;

import com.example.davr.entity.Chat;
import com.example.davr.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface ChatRepository extends JpaRepository<Chat, Long> {

    Optional<Chat> findByDeletedFalseAndId(Long id);

    @Query("select c from Chat c where :sender member c.users and :receiver member c.users")
    Optional<Chat> findChat(@Param("sender") Users sender, @Param("receiver") Users receiver);





}
