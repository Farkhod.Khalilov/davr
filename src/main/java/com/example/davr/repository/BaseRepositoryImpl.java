package com.example.davr.repository;

import com.example.davr.entity.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

public class BaseRepositoryImpl<T extends BaseEntity, ID> extends SimpleJpaRepository<T, ID> implements BaseRepository<T, ID> {
    private final Specification<T> isNotDeletedSpecification = (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.<Boolean>get("deleted"), false);
    private final EntityManager entityManager;

    public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public T trash(ID id) {
        T one = findById(id).orElseThrow();//TODO triget yzila
        one.setDeleted(true);
        return save(one);
    }

    @Override
    public List<T> findAllNotDeleted() {
        return findAll(isNotDeletedSpecification);
    }

    @Override
    public Page<T> findAllNotDeleted(Pageable pageable) {
        return findAll(isNotDeletedSpecification, pageable);
    }
}
