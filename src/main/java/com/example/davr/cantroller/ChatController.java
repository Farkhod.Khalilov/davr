package com.example.davr.cantroller;

import antlr.CharFormatter;
import com.example.davr.dto.ChatDto;
import com.example.davr.dto.ChatFormDto;
import com.example.davr.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/chat")
public class ChatController {


    private final ChatService chatService;


    @GetMapping("user")
    public ChatDto getUserChat(@RequestBody ChatFormDto dto) {
        return chatService.getChat(dto);
    }
}
