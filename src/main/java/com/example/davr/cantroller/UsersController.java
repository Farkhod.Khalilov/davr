package com.example.davr.cantroller;

import com.example.davr.dto.UsersDto;
import com.example.davr.dto.UsersForm;
import com.example.davr.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/users")
@RequiredArgsConstructor
public class UsersController {

    private final UsersService service;


    @PostMapping
    public UsersDto add( @Valid @RequestBody UsersForm form){
        return service.add(form);
    }

    @GetMapping("/{id}")
    public UsersDto getOne(@PathVariable Long id){
        return service.getOne(id);
    }

    @GetMapping
    public List<UsersDto> findAll(){
        return service.findAll();
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        service.delete(id);
    }
    @PutMapping("/{id}")
    public UsersDto changePassword(@PathVariable Long id, @Valid @RequestBody UsersForm  dto){
        return service.changePassword(id,dto);
    }

    @PutMapping("update/{id}")
    public UsersDto update(@PathVariable Long id,@RequestBody UsersForm form){
        return service.update(form, id);
    }









}
