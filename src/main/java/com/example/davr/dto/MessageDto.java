package com.example.davr.dto;

import com.example.davr.entity.Message;
import lombok.*;
import static com.example.davr.extensions.Extensions.dateFormat;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageDto {

    private Long chatId;

    private String author;

    private String text;

    private String created_at;


}
