package com.example.davr.dto;

import com.example.davr.entity.Chat;
import com.example.davr.entity.Users;
import com.example.davr.extensions.Extensions;
import lombok.*;

import java.util.stream.Collectors;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatDto {

    private Long id;

    private String name;

    public static ChatDto toDto(Chat chat) {
        return ChatDto.builder()
                .name(chat.getName())
                .id(chat.getId())
                .build();
    }
}


