package com.example.davr.dto;

import com.example.davr.entity.Users;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class ChatFormDto {
    private Long senderId;
    private Long receiverId;
}
