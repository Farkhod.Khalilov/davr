package com.example.davr.dto;

import com.example.davr.entity.Users;
import com.example.davr.enums.UserRole;
import com.example.davr.extensions.Extensions;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.Date;

import static com.example.davr.extensions.Extensions.dateFormat;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsersForm {
    @NotBlank(message = "username not null")
    private String username;

    @NotBlank(message = "password not null")
    private String password;

    private UserRole role;

    public static Users toEntity(UsersForm form) {
        return Users.builder()
                .username(form.getUsername())
                .password(form.getPassword())
                .build();
    }
}
