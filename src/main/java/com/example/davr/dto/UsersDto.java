package com.example.davr.dto;
import com.example.davr.entity.Users;
import com.example.davr.extensions.Extensions;
import lombok.*;

import static com.example.davr.extensions.Extensions.dateFormat;


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsersDto {

    private Long id;

    private String username;

    private String created_at;

    public static UsersDto toDto(Users users){
        return UsersDto.builder()
                .id(users.getId())
                .username(users.getUsername())
                .created_at(dateFormat(users.getCreatedAt()))

                .build();
    }
}
