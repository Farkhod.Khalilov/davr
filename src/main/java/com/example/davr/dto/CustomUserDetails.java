package com.example.davr.dto;

import com.example.davr.entity.Users;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@AllArgsConstructor(staticName = "of")
public class CustomUserDetails implements UserDetails {

    private Users user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(user.getRole().name()));//TODO GrantedAuthority ISHLATILGAN SHUGA XAM ETIBOR BERGIN QOLGAN PROJECTLARDA UNAQA EMAS
        // @Override
        //    public Collection<? extends GrantedAuthority> getAuthorities() {
        //        return Collections.singleton(new SimpleGrantedAuthority(user.getRole().name()));
        //    }
    }

    public Users getUser() {
        return this.user;
    }

    @Override
    public String getPassword() { //TODO password yo'q user entityda
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
