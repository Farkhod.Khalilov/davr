package com.example.davr.exceptions;

public enum ErrorCode {
    USER_NOT_FOUNT("Couldn't find %s username!", 100),
    CHAT_NOT_REQUEST_STATUS("CHAT_NOT_REQUEST_STATUS", 108);

    public final String message;
    public final Integer code;

    ErrorCode(String message, Integer code) {
        this.message = message;
        this.code = code;
    }
}
