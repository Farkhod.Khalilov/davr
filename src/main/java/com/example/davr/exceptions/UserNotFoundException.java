package com.example.davr.exceptions;

import lombok.Getter;

@Getter
public class UserNotFoundException {

    private final String username;

    public UserNotFoundException(String username){
        this.username=username;
    }



}
