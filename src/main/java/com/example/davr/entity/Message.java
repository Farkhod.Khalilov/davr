package com.example.davr.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
public class Message extends BaseEntity {

    private String chat;

    @ManyToOne
    private Users author;

    private String text;


}
