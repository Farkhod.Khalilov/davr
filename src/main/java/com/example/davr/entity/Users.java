package com.example.davr.entity;

import com.example.davr.enums.UserRole;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Users extends BaseEntity {

    @Column(unique = true, length = 128)
    private String username;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(length = 64)
    private UserRole role;

}

