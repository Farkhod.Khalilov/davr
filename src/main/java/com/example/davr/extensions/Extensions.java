package com.example.davr.extensions;

import lombok.Data;
import lombok.val;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;
import java.util.function.Supplier;

public class Extensions extends Throwable {

    public static String dateFormat(Date date) {
        val formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return runIfNonNull(date, formatter::format);
    }

    public static <T, R> R runIfNonNull(T it, Function<T, R> fun) {
        if (it != null) return fun.apply(it);
        return null;
    }
}