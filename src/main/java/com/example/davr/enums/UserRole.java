package com.example.davr.enums;

public enum UserRole {
    ADMIN, MODERATOR, CLIENT
}

